from flask import Blueprint, render_template

from monolith.database import db, Project, Group
from monolith.auth import current_user


home = Blueprint('home', __name__)


@home.route('/')
def index():
    if current_user is not None and hasattr(current_user, 'id'):
        projects = db.session.query(Project).filter(Project.user_id == current_user.id)
    else:
        projects = None
    return render_template("index.html", projects=projects)


@home.route('/groups')
def index_groups():
    if current_user is not None and hasattr(current_user, 'id'):
        groups = db.session.query(Group).filter(Group.user_id == current_user.id)
    else:
        groups = None
    return render_template("groups.html", groups=groups)
